<?php

namespace Orchestrate\Kernel\Translate;

/**
 * The text class can be used for translating text phrases into the given locale.
 *
 */
class Text implements \JsonSerializable
{
    /**
     * String for rendering
     *
     * @var string
     */
    private $text;

    /**
     * Arguments for placeholder values
     *
     * @var array
     */
    private $arguments;

    /**
     * Text construct
     *
     * @param string $text
     * @param array $arguments
     */
    public function __construct($text, array $arguments = [])
    {
        $this->text = (string)$text;
        $this->arguments = $arguments;
    }

    /**
     * Get the base untranslated text.
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Get arguments for placeholder values.
     *
     * @return array
     */
    public function getArguments()
    {
        return $this->arguments;
    }

    /**
     * Renders text
     *
     * @return string
     */
    public function render()
    {
        try {
            return Renderer::getRenderer()->render([$this->text], $this->getArguments());
        } catch (\Exception $e) {
            return $this->getText();
        }
    }

    /**
     * Defers rendering to the last possible moment (when converted to string)
     *
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * Specify data which should be serialized to JSON.
     *
     * @return string
     */
    public function jsonSerialize()
    {
        return $this->render();
    }
}
