<?php

namespace Orchestrate\Kernel\Translate;

use Orchestrate\Kernel\Translate\Renderer\Placeholder;

/**
 * Base renderer class. Holds the renderer to use for rendering texts.
 */
class Renderer
{
    /**
     * The renderer to use.
     *
     * @var RendererInterface
     */
    private static $renderer;

    /**
     * Set the text renderer to use
     *
     * @param RendererInterface $renderer
     * @return void
     */
    public static function setRenderer(RendererInterface $renderer)
    {
        self::$renderer = $renderer;
    }

    /**
     * Returns the text renderer. If no text renderer is set, will return the placeholder renderer
     *
     * @return RendererInterface
     */
    public static function getRenderer()
    {
        if (!self::$renderer) {
            self::$renderer = new Placeholder();
        }
        return self::$renderer;
    }
}
