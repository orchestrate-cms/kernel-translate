<?php

namespace Orchestrate\Kernel\Translate\Renderer;

use Orchestrate\Kernel\Translate\RendererInterface;

/**
 * The composite renderer gives the ability to use multiple renderer classes.
 *
 */
class Composite implements RendererInterface
{
    /**
     * @var RendererInterface[]
     */
    protected $renderers;

    /**
     * @param RendererInterface[] $renderers
     * @throws \InvalidArgumentException
     */
    public function __construct(array $renderers)
    {
        foreach ($renderers as $renderer) {
            if (!$renderer instanceof RendererInterface) {
                throw new \InvalidArgumentException(
                    sprintf('Instance of the phrase renderer is expected, got %s instead.', get_class($renderer))
                );
            }
        }
        $this->renderers = $renderers;
    }

    /**
     * Renderer source text
     *
     * @param [] $source
     * @param [] $arguments
     * @return string
     */
    public function render(array $source, array $arguments = [])
    {
        $result = $source;
        foreach ($this->renderers as $render) {
            $result[] = $render->render($result, $arguments);
        }
        return end($result);
    }
}
