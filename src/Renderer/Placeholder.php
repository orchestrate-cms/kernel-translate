<?php

namespace Orchestrate\Kernel\Translate\Renderer;

use Orchestrate\Kernel\Translate\RendererInterface;

/**
 * The placeholder renderer does not translate the text. It will only replace the placeholders in the default text
 * with the given parameters. The placeholder renderer is the default renderer when a renderer is not specified.
 *
 */
class Placeholder implements RendererInterface
{
    /**
     * Renderer source text
     *
     * @param [] $source
     * @param [] $arguments
     * @return string
     */
    public function render(array $source, array $arguments)
    {
        $text = end($source);

        if ($arguments) {
            $placeholders = array_map([$this, 'keyToPlaceholder'], array_keys($arguments));
            $pairs = array_combine($placeholders, $arguments);
            $text = strtr($text, $pairs);
        }

        return $text;
    }

    /**
     * Get key to placeholder
     *
     * @param string|int $key
     * @return string
     * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
     */
    private function keyToPlaceholder($key)
    {
        return '%' . (is_int($key) ? strval($key + 1) : $key);
    }
}
