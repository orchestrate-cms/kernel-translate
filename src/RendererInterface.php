<?php

namespace Orchestrate\Kernel\Translate;

/**
 * The renderer interface is used to define a renderer class. Renderer classes are used for rendering texts.
 *
 */
interface RendererInterface
{
    /**
     * Renderer source text
     *
     * @param [] $source
     * @param [] $arguments
     * @return string
     */
    public function render(array $source, array $arguments);
}
